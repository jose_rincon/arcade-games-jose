import random

def print_hello():
    print("Hell")

def print_goodbye():
    print("Bye!")

def print_welcome_message():
    print("Welcome to Camel")
    print("You have stolen a camel to make your way across the great Mobi desert")
    print("The natives want their camel back and are chasing you down! Survive your")
    print("desert trek and outrun the natives")
    print()
    print()


def main():
    print_welcome_message()
    done = False
    miles_traveled = 0
    thirst_level = 0
    camel_tiredness = 0
    distance_natives_have_traveled = -20
    drinks_in_canteen = 3

    while not done:

        #print()
        #print("MilesT: " + str(miles_traveled))
        #print("ThirstL: " + str(thirst_level))
        #print("camel_tiredness: " + str(camel_tiredness))
        #print("Distance Natives have traveled: " + str(distance_natives_have_traveled))
        #print("Drinks in Canteen: " + str(drinks_in_canteen))
        #print()
        #print()

        print("A. Drink from your canteen")
        print("B. Move ahead at a moderate speed")
        print("C. Move ahead at full speed")
        print("D. Stop for the night and rest")
        print("E. Status Check")
        print("Q. Quit")
        print()

        user_choice = input("Enter your choice and then press enter:  ")

        if user_choice.upper() == "Q" :
            print("You're Quitting!")
            print("Quick Stats")
            print("Miles traveled: " + str(miles_traveled))
            print("Drinks in canteen: " + str(drinks_in_canteen))
            print("The Natives are " + str(distance_natives_have_traveled + miles_traveled) + " miles behind you")
            done = True

        elif user_choice.upper() == "A":
            print("You're drinking from your canteen")
            if drinks_in_canteen > 0:
                print("There are drinks in the canteen")
                print("Taking one drink")
                drinks_in_canteen = drinks_in_canteen - 1
                thirst_level = 0
            else:
                print("Sorry, there are no drinks in the canteen")

        elif user_choice.upper() == "B":
            print("You're moving ahead at a moderate pace")
            ahead = random.randint(5, 12)
            print("You're moving " + str(ahead) + " miles ahead")
            print()
            miles_traveled = miles_traveled + ahead
            thirst_level = thirst_level + 1
            camel_tiredness = camel_tiredness + random.randint(1, 3)
            distance_natives_have_traveled += random.randint(7, 14)

        elif user_choice.upper() == "C":
            print("You're moving ahead at full speed")
            ahead = random.randint(10,20)
            print("You're moving " + str(ahead) + " miles ahead")
            print()
            miles_traveled = miles_traveled + ahead
            thirst_level = thirst_level + 1
            camel_tiredness = camel_tiredness + random.randint(1,3)
            distance_natives_have_traveled = distance_natives_have_traveled + random.randint(7,14)

        elif user_choice.upper() == "D":
            print("You're Stopping for the night")
            camel_tiredness = 0
            print("Your camel is really happy!")
            distance_natives_have_traveled = distance_natives_have_traveled + random.randint(7,14)
            print()

        elif user_choice.upper() == "E":
            print("Doing a status check")
            print("Miles traveled " + str(miles_traveled))
            print("Drinks in canteen: " + str(drinks_in_canteen))
            print("Camel Tiredness: " +  str(camel_tiredness))
            print("Thirst Level: " + str(thirst_level))

        if thirst_level >= 4:
            print("You're thirsty!")
            print("Drink some water!")
            print("You'll die soon, so drink up!")
            print()
        elif thirst_level >= 6:
            done = True
            print("You died of thirst! Bye bye")
            print()

        if camel_tiredness > 5:
            print("Your Camel is getting tired! Rest up")
            print()

        if camel_tiredness > 8:
            print("Your Camel died!. Bye bye")
            print()
            done = True

        if distance_natives_have_traveled == 0:
            print("The natives have caught up! You're dead!")
            print()

        if miles_traveled - distance_natives_have_traveled < 15:
            print("The Natives are getting close!")
            print("Move move move!")
            print()

        if miles_traveled > 200:
            print("You won!")
            done = True


if __name__ == "__main__":
    main()

